<?php

namespace vmcl\Laravel\Encryption;

use Illuminate\Support\ServiceProvider;

/**
 * Class EncryptionServiceProvider
 *
 * @package Tomgrohl\Laravel\Encryption
 */
class EncryptionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('encrypter', function($app)
        {
            if ($app['config']->has('app.cipher'))
            {
                return new Encrypter(
                    $app['config']['app.key'],
                    $app['config']['app.cipher']
                );
            }

            return new Encrypter($app['config']['app.key']);
        });
    }
}
